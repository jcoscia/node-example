FROM node:6.17.0-alpine

EXPOSE 8080

RUN apk add --update curl && rm -rf /var/cache/apk/*

WORKDIR /app

COPY   *  /app/

CMD ["npm" ,"start"]